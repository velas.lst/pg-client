// https://nuxt.com/docs/api/configuration/nuxt-config
const ONE_DAY = 60 * 60 * 24 * 1000;

export default defineNuxtConfig({
    ssr: true,
    devtools: { enabled: true },

    // modules: ["@pinia/nuxt"],
    app: {
		pageTransition: { name: 'page', mode: 'out-in' },
		layoutTransition: { name: 'layout', mode: 'out-in' },
		head: {
			title: 'Aplicaciones de la salud',
			link: [
				{ rel: 'stylesheet', href: '/styles/main.css' },
				{ rel: 'stylesheet', href: '/styles/menu.css' },
				{ rel: 'stylesheet', href: '/styles/footer.css' },
			]
		}
	},
    runtimeConfig: {
		// Private keys are only available on the server
		apiSecret: '',
		// algo aqui
		// Public keys that are exposed to the client
		public: {
			session: {
				value: ''
			},
		  	apiBase: 'https://app-gestion-citas-back.onrender.com/api/v1'
		},
		cookieName: "__session",
		cookieSecret: "secret",
		cookieExpires: ONE_DAY.toString(),
		cookieRememberMeExpires: (ONE_DAY * 7).toString(),
	},
});