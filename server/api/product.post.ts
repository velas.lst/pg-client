import { useFetch } from "nuxt/app";
import { objectCallable } from "../utils/ObjectCallableForReponses";
import { deleteRec, insertRec, updateRec } from "../utils/forms/CommonCrud";

export default defineEventHandler(async (event) => {
    const body = await readBody(event);

    const entries:objectCallable = {
        deleteRecord: async function() {
            let r = await deleteRec('product', parseInt(body.id));
            return r;
        },
        updateRecord: async function() {
            body.precioUnitario = parseFloat(body.precioUnitario);
            let r  = await updateRec('product', body);
            return r;
        },
        insertRecord: async function() {
            body.precioUnitario = parseFloat(body.precioUnitario);
            let r = await insertRec('product', body);
            console.log(r);
            return r;
    
        },
    };
    return await entries[(event.headers.get('method') || '').toString()]();
});