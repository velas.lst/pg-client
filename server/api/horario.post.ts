import { useFetch } from "nuxt/app";
import { objectCallable } from "../utils/ObjectCallableForReponses";
import { deleteRec, insertRec, updateRec } from "../utils/forms/CommonCrud";
import { timeToFullDate } from "../utils/common/date-and-time";

export default defineEventHandler(async (event) => {
    let body = await readBody(event);
    body.doctorId = parseInt(body.doctorId);
    body.horaInicio = timeToFullDate(body.horaInicio);
    body.horaFin = timeToFullDate(body.horaFin);
    console.log(body);
    const entries:objectCallable = {
        deleteRecord: async function() {
            let r = await deleteRec('horario', parseInt(body.id));
            return r;
        },
        updateRecord: async function() {
            let r  = await updateRec('horario', body);
            return r;
        },
        insertRecord: async function() {    
            let r = await insertRec('horario', body);
            return r;
        },
    };
    return await entries[(event.headers.get('method') || '').toString()]();
});