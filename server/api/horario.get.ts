import { objectCallable } from "~/server/utils/ObjectCallableForReponses";


export default defineEventHandler(async (event) => {
    const runtimeConfig  = (useRuntimeConfig());

    const entries:objectCallable = {
        getByMedicId: async function () {
            const query = getQuery(event);
            let response = await $fetch(`${runtimeConfig.public.apiBase}/horario/by-medic-id/${query.id}`);
            return response;
        },        
        list: async function () {
            let response = await $fetch(`${runtimeConfig.public.apiBase}/horario`);
            return response;
        },
        findRecord: async function() {
            const query = getQuery(event)
            let response = await $fetch(`${runtimeConfig.public.apiBase}/horario/${query.id}`);
            return response;
        },
    };
    return await entries[(event.headers.get('method') || '').toString()]();
});