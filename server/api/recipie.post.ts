import { useFetch } from "nuxt/app";
import { objectCallable } from "../utils/ObjectCallableForReponses";
import { deleteRec, insertRec, updateRec } from "../utils/forms/CommonCrud";

export default defineEventHandler(async (event) => {
    let bd = await readBody(event);
    
    bd.productIds = bd.productos
    .filter((i: any) => i.chk)
    .map((i: any) => i.id.toString());
    bd.totalPrice = parseFloat(bd.totalPrice);
    bd.userId = parseInt(bd.user.id);
    const { createdAt, updatedAt, user, productos, ...body } = bd;

    console.log(body);
    const entries:objectCallable = {
        deleteRecord: async function() {
            let r = await deleteRec('receta', parseInt(body.id));
            return r;
        },
        updateRecord: async function() {
            let r  = await updateRec('receta', body);
            return r;
        },
        insertRecord: async function() {
            let r = await insertRec('receta', body);
            return r;
        },
    };
    return await entries[(event.headers.get('method') || '').toString()]();
});