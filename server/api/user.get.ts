import { objectCallable } from "../utils/ObjectCallableForReponses";

export default defineEventHandler(async (event) => {
    const runtimeConfig  = (useRuntimeConfig());

    const entries:objectCallable = {
        list: async function () {
            let response = await $fetch(`${runtimeConfig.public.apiBase}/user`);
            return response;
        },
        findRecord: async function() {
            const query = getQuery(event)
            let response = await $fetch(`${runtimeConfig.public.apiBase}/user/${query.id}`);
            return response;
        },
    };
    return await entries[(event.headers.get('method') || '').toString()]();
});