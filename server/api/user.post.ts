import { useFetch } from "nuxt/app";
import { objectCallable } from "../utils/ObjectCallableForReponses";
import { deleteRec, insertRec, updateRec } from "../utils/forms/CommonCrud";

export default defineEventHandler(async (event) => {
    const body = await readBody(event);

    const entries:objectCallable = {
        deleteRecord: async function() {
            let r = await deleteRec('user', parseInt(body.id));
            return r;
        },
        updateRecord: async function() {
            let r  = await updateRec('user', body);
            return r;
        },
        insertRecord: async function() {
            let r = await insertRec('user', body);
            console.log(r);
            return r;
    
        },
    };
    return await entries[(event.headers.get('method') || '').toString()]();
});