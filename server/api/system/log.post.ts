import { useCM } from "~/composables/useCM";
import { objectCallable } from "~/server/utils/ObjectCallableForReponses";
import { serialize } from "~/server/utils/cookie";

export default defineEventHandler(async (event) => {
    let body = await readBody(event);
    const config = useRuntimeConfig();
    const cookieManager = useCM();

    const runtimeConfig  = (useRuntimeConfig());

    let methods:objectCallable = {
        in: async function () {
            let r:any = await $fetch(`${runtimeConfig.public.apiBase}/user/login`, {
                method: 'POST',
                body: body
            });
            if (r.res === true) {
                const { contra, ...userInfo } = r.data;
                // cookieManager.create(event, config.cookieName, userInfo);
                const _cookieUser = setCookie(event, '_user', serialize(userInfo));
                return { user: userInfo }
            }
            return { user: null };
        },
        out: function () {
            cookieManager.destroy(event, config.cookieName);
        }
    };
    return await methods[(event.headers.get('method') || '').toString()]();
});