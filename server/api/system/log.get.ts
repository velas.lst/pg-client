import { objectCallable } from "~/server/utils/ObjectCallableForReponses";

export default defineEventHandler(async (event) => {
    const query = getQuery(event);
    let methods:objectCallable = {
        readCookies: function () {
            const userCookies:string|undefined = getCookie(event, '_user');
            if (userCookies === undefined) {
                return { user: null };
            }
            console.log(userCookies);
            let cookie = deserialize(userCookies as string);
            return { user: cookie };
        },
    };
    return methods[(query.method || 'readCookies').toString()]();
});