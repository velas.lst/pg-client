type userForRecipie = {
    id: number,
    nombres: string,
    apellidos: string,
    edad: string,
    type: string
}

type productForRecipie = {
    id: number,
    nombre: string
    descripcion: string
    cantidadDisponible: number,
    precioUnitario: string
    proveedor: string
}


export type recipieType = {
    id: number,
    nombre: string,
    totalPrice: string,
    userId: number,
    createdAt: string,
    updatedAt: string,
    user: userForRecipie,
    productos: Array<productForRecipie>
}
