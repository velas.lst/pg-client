const runtimeConfig  = (useRuntimeConfig());

export async function deleteRec(directive: string, id: number) {
    let r = await $fetch(`${runtimeConfig.public.apiBase}/${directive}/${id}`, {
        method: 'DELETE',
    });
    return r;
}
export async function updateRec(directive: string, body: any) {
    let r = await $fetch(`${runtimeConfig.public.apiBase}/${directive}/${body.id}`, {
        method: 'PATCH',
        body: body
    });
    return r;
}
export async function insertRec(directive: string, body: any) {
    let r = await $fetch(`${runtimeConfig.public.apiBase}/${directive}`, {
        method: 'POST',
        body: body
    });
    return r;
}