const actions = [
    { index: 0, name: 'Guardar', method: 'insertRecord', }, 
    { index: 1, name: 'Actualizar', method: 'updateRecord', }, 
    { index: 2, name: 'Eliminar', method: 'deleteRecord', }, 
];

export function baseActionDetection(id: number, route: any) {
    if ((id ?? 0) <= 0 || isNaN(id)) {
        return actions[0];
    }
    return actions[parseInt(route.query.a?.toString() ?? '1')];

}