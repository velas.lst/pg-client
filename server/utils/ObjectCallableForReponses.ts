export type objectCallable = {
    [idx: string]: CallableFunction
};