// const { KeyObject, createHmac, timingSafeEqual } = await import("node:crypto");


export type CookieSecret = string|Buffer/* |KeyObject */;

export function serialize(obj: any) {
    let _obj = Object.assign({}, obj);
    const value = btoa(JSON.stringify(_obj));
    return value;
}

export function deserialize(value: string) {
    return JSON.parse(atob(value));
}

export function sign(value: string, _secret: CookieSecret) {
    return `${value}.${value}`;
    /* const createHmac = (async () => {
        const { createHmac } = await import('crypto');
        return createHmac;
    })() as any;
    const signature = createHmac("sha256", secret).update(value).digest("base64").replace(/=+$/, "");

    return `${value}.${signature}`; */
}

export function unsign(input: string, secret: CookieSecret) {
    const value = input.slice(0, input.lastIndexOf("."));
    const expectedInput = sign(value, secret);
    const expectedBuffer = Buffer.from(expectedInput);
    const inputBuffer = Buffer.from(input);

    // const { KeyObject, createHmac, timingSafeEqual } = await import("node:crypto");

    const timingSafeEqual = (async () => {
        const { timingSafeEqual } = await import('crypto');
        return timingSafeEqual;
    })() as any;

    if (!(expectedBuffer.equals(inputBuffer) && timingSafeEqual(expectedBuffer, inputBuffer))) {
        throw createError({
            statusCode: 400,
            statusMessage: "Invalid cookie signature",
            message: "Invalid cookie signature",
        });
    }

    return value;
}