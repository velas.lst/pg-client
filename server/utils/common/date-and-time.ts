export function forceToTime(fecha: string) : string {
    return (new Date(fecha)).toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' });
}

export function timeToFullDate(time: string) {
    let timeComponents:Array<string> = time.split(':');
    const fechaActual:Date = new Date();
    const fechaDeseada:Date = new Date(fechaActual.getFullYear(), fechaActual.getMonth(), fechaActual.getDate(), parseInt(timeComponents[0]), parseInt(timeComponents[1]), 0);
    return fechaDeseada.toISOString();
}