import { useState } from "nuxt/app";
import { objectCallable } from "../ObjectCallableForReponses";

function createDefaultAuthData() {
    return {
        uname: '',
        isLogged: false,
    };
}

const auth = useState('auth', ():any => createDefaultAuthData());

export function SessionMan() : objectCallable {
    let instance: objectCallable = {
        destroy: function () {
            auth.value = createDefaultAuthData();
        },
        get: function (keyName:string) : any  {
            return auth.value[keyName] || undefined;
        },
        set: function (keyName:string, val:any) : void {
            auth.value[keyName] = val;
        }
    };
    return instance;
}