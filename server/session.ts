import { eventHandler } from 'h3';

// Return all session data to the frontend
export default defineEventHandler(event => event.context.session);