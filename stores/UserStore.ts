import { defineStore } from 'pinia'


export const useUserStore = defineStore('user', {
	state: () => ({
		uname: '',
	}),
  	getters: {
    	get: (state) => state.uname,
  	},
	actions: {
		updateVar(value: any) {
			this.uname = value;
		}
	},
});