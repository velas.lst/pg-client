import axios from "axios";
import { serialize, sign, type CookieSecret } from "~/server/utils/cookie";

const CommonCookieConfig:any = {
    httpOnly: true,
    path: "/",
    sameSite: "strict",
    secure: process.env.NODE_ENV === "production",
};

export const useCM = () => {
    const instance = {
        create: function (ev: any, cookieName: string, info:any) {
            const config = useRuntimeConfig();
            const session = serialize(info);
            const signedSession = sign(session, config.cookieSecret as CookieSecret);
            setCookie(ev, cookieName, signedSession, {
                httpOnly: CommonCookieConfig.httpOnly,
                path: CommonCookieConfig.path,
                sameSite: CommonCookieConfig.sameSite,
                secure: CommonCookieConfig.secure,
                expires: undefined
            });
            // const { password: _password, ...userWithoutPassword } = info;
        },
        set: function () {

        },
        get: function (event:any, cookieName:string) {
            try {
                return getCookie(event, cookieName);
            } catch (error) {
                return null;
            }
        },
        delete: function () {

        },
        destroy: function (event: any, cookieName: string) {
            deleteCookie(event, cookieName, CommonCookieConfig);
        },
        getFromClient: async function (cookieName: string = '') {
            let r = await axios.get(`api/system/log?c=${cookieName}`);
            return r;
        },
        destroyFromClient: async function () {
            let r = await axios.post('api/system/log', undefined, {
                headers: {
                    'Content-Type': 'application/json',
                    'method': 'out'
                }
            });
            return r;
        }
    }
    return instance;
};
