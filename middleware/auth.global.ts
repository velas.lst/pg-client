import type { RouteLocationRaw } from "vue-router";

export default defineNuxtRouteMiddleware(async (to) => {
    function redirectToRoute(targetRoute:string) : any {
        // if (process.client) {
        window.location.href = targetRoute;
        // }
        /* if (process.server) {
        } */
        // return navigateTo(targetRoute) as Promise<any>;
        // return null;
    }


    let response:any = await $fetch("/api/system/log?method=readCookies");
    let isLoginPage = (to.name === 'login'); 
    if (response.user !== null) {
        if (isLoginPage) {
            return redirectToRoute('/');
        }
    } else {
        if (!isLoginPage) {
            return redirectToRoute('/login');
        }
    }
});